# TP 4 RX - Socket Chat Bot
> Samuel Brun, Romain Guyot, Alexandre Lithaud

***Ce projet est uniquement destiné à des fins éducatives***

---

- [TP 4 RX - Socket Chat Bot](#tp-4-rx---socket-chat-bot)
- [Fonctionnement](#fonctionnement)
  - [**Lancement**](#lancement)
  - [**Features**](#features)
  - [**Structure de données**](#structure-de-données)
  - [**Limites**](#limites)
  - [**Exemple**](#exemple)
  - [**Test**](#test)

---

# Fonctionnement

## **Lancement**

La commande **make** compile le projet ainsi que les programmes de tests.

Pour lancer le serveur il suffie de faire la commande `./server <numPort>` dans un terminal par défaut le port est 9999.

Pour lancer le client il suffie de faire la commande `./client <numPort>` das un autre terminal par défaut le port d'écoute est 9999.

Une fois connecté le client devra choisir un pseudo a 6 lettres maximum. Tant que le pseudo n'est pas bon alors le code redemandera le nom du client.

En tant que client vous avez plusieur options :

- Vous abonnez
- Vous désabonnez
- Ecrire un message
- Afficher la liste des abonnement
- Quitter


Pour vous abonnez : vous pouvez écrire `Subscribe/subscribe/sub/Sub/s/S`
> Puis ensuite vous pouvez écrire le pseudo de la personne a vous abonner.

Pour vous desabonnez : vous pouvez écrire `Unsubscribe/unsubscribe/unsub/Unsub/u/U`
> Puis ensuite vous pouvez écrire le pseudo de la personne a vous desabonner.
 
Pour afficher un message : vous pouvez écrire `write/Write/w/W`
> Puis ensuite vous pouvez écrire le message que vous voulez

Pour afficher la liste des abonnement : vous pouvez écrire `list/List/l/L`

Pour quitter le programme : vous pouvez écrire `Quit/quit/Q/q` 

Enfin si vous ne savez plus quelle commande réaliser vous pouvez écrire `Help/help/h/H` pour afficher l'aide.

## **Features**

Nous avons implémenté les fonctionnalités demandés dans l'énoncé tel que l'envoie de messsage en direct ou différer lors de la reconnexion du client destination. de plus si le message est envoyé a tous le monde, il est supprimé.

## **Structure de données**

Nous avons impplémenté une interface de données qui nous permet de stocker sous forme de liste chainée les personnes avec :
- pseudo
- list des abonnements
- list des abonnées
- list des messages de la personne

Pour cela on a implémenté une structure de liste de pseudo, qui est juste une liste chainée contenant des pseudo.

Parallelement, on maintient une liste de clients connectée avec le pseudo et la socket qui permet de communiquer avec ce dernier.

## **Limites**

Le programme semble alergique a gcc. En effet en utilisant le compilateur clang (par défaut sur mac), le programme s'execute sans soucis. En revanche en utilisant gcc, des la connexion du client, la fonction accept plante et provoque un crash du serveur.
Nous n'avons pas traiter une fin propre apres un controle C (via les signaux).

## **Exemple**

./serveur   
./client 
  pseudo : a
./client : b
  commande : sub
    puis a
  commande quit

sur a : 
  commande : w
    bonjour je suis a

./client : b
  recoit : a > bonjour je suis a

## **Test**

Les fichiers Test_interface_personne et Test_list_personne nous ont permis de tester les structures. Pour le reste, nous avons fait des tests a la main en essayant de stresser le systeme pour le faire entrer dans des etats incohérent.