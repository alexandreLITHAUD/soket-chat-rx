#include "interface.h"

int main(){


    printf("========================\n");
    printf("=     Test de j'ajout    =\n");
    printf("========================\n");

    addPersonne("jdg");
    addPersonne("doigby");
    addPersonne("sazuke");
    addPersonne("talus");
    
    printInscrits();

    if(getPersonne("jdg")==NULL) { return EXIT_FAILURE; }
    if(getPersonne("doigby")==NULL) { return EXIT_FAILURE; }
    if(getPersonne("sazuke")==NULL) { return EXIT_FAILURE; }
    if(getPersonne("talus")==NULL) { return EXIT_FAILURE; }

    printf("========================\n");
    printf("= Test de message      =\n");
    printf("========================\n");


    addMessage("jdg","Mais etait sur");
    addMessage("jdg","les jeux");
    addMessage("jdg","MIAMMM");
    addMessage("jdg","TAKE ON MEEE");
    addMessage("doigby","Je plle D");
    addMessage("sazuke","malvalame");
    addMessage("talus","Antoine");
    addMessage("talus","STACKLORD");
    
    printMessages("jdg");
    printMessages("doigby");
    printMessages("sazuke");
    printMessages("talus");

    delPersonne("doigby");
    //delPersonne("jdg");
    //delPersonne("talus");
    //delPersonne("sazuke");
    printInscrits();
    
    //tests addAbonnement
    
    printf("========================\n");
    printf("= Test de addAbonnement =\n");
    printf("========================\n");

    addAbonnement("sazuke", "jdg");
    addAbonnement("Dio", "jdg");
    addAbonnement("talus", "jdg");

    //verif de la cible
    printf("-------------------------\n");
    printf("Liste des abonnés de JDG ('sazuke et talus' attendus):\n");
    printPersonneList(getAbonnes("jdg"));
    printf("Liste des abonnements de JDG (aucun attendu):\n");
    printPersonneList(getAbonnements("jdg"));

    //verif d'un abonne 
    printf("-------------------------\n");
    printf("Liste des abonnés de sazuke (aucun attendu):\n");
    printPersonneList(getAbonnes("sazuke"));
    printf("Liste des abonnements de sazuke ('jdg' attendu):\n");
    printPersonneList(getAbonnements("sazuke"));


    printf("========================\n");
    printf("= Test de removeAbonnement =\n");
    printf("========================\n");

    //test du retrait d'abonnement
    removeAbonnement("sazuke", "jdg");

    //verif cible
    printf("-------------------------\n");
    printf("Liste des abonnés de JDG ('talus' attendu):\n");
    printPersonneList(getAbonnes("jdg"));

    //verif abonne
    printf("-------------------------\n");
    printf("Liste des abonnements de sazuke (aucun attendu):\n");
    printPersonneList(getAbonnements("sazuke"));
    
    return EXIT_SUCCESS;
}