/******************************************************************************/
/*			Application: ....			              */
/******************************************************************************/
/*									      */
/*			 programme  SERVEUR 				      */
/*									      */
/******************************************************************************/
/*									      */
/*		Auteurs :  ....						      */
/*		Date :  ....						      */
/*									      */
/******************************************************************************/	

#include<stdio.h>
#include <curses.h>

#include<sys/signal.h>
#include<sys/wait.h>
#include<stdlib.h>
#include<string.h>

#include "fon.h"     		/* Primitives de la boite a outils */
#include "list.h"
#include "interface.h"

#define SERVICE_DEFAUT "9999"

#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

void serveur_appli (char *service);   /* programme serveur */
int connectServerClient(char *service, struct sockaddr_in *serveurIP);
void sendMessage(int sockSrc, list clientList,char* buf);
void sendMessagesConnect(int sockSrc,char* pseudo);

/******************************************************************************/	
/*---------------- programme serveur ------------------------------*/

int main(int argc,char *argv[])
{

	char *service= SERVICE_DEFAUT; /* numero de service par defaut */


	/* Permet de passer un nombre de parametre variable a l'executable */
	switch (argc)
 	{
   	case 1:
		  printf("defaut service = %s\n", service);
		  		  break;
 	case 2:
		  service=argv[1];
            break;

   	default :
		  printf("Usage:serveur service (nom ou port) \n");
		  exit(1);
 	}

	/* service est le service (ou numero de port) auquel sera affecte
	ce serveur*/
	serveur_appli(service);
}


/******************************************************************************/	
/**
 * It's a server that listens for new connections and handles them
 * 
 * @param service the name of the service to connect to.
 */
void serveur_appli(char *service)
{
	int numSocket;
	struct sockaddr_in *serveurIP;
	numSocket = connectServerClient(service,serveurIP);

	int newNumSocket;
    int numSubs;

	char pseudo[7];
    char persoASub[7];
    char bufferText[21];

    /* Init pour le select */
    /*Structure*/
    fd_set rfds;
    FD_ZERO(&rfds);//mise a 0
    FD_SET(numSocket,&rfds); // ajout socket de connexion
    int retVal;

    /* check des clients (iste des clients connectés a l'instant T) */
    list clientList = NULL;
    int nbClientCourant = 0;
    /* Liste des clients que demande la déconnexion (remplise et vidée a chaque tour de boucle)*/
    list removeToClientList = NULL;
    int nbRemoveToClientList = 0;
    
    /* Boucle de taitement */
	while(1){
        /* On regarde si il y a quelque chose a faire (un descripteur a des trucs a lire) */
        
        retVal = select(FD_SETSIZE,&rfds,NULL,NULL,NULL);
        
        if(retVal < 0){/* Echec du select */
            perror("Err Select");
            break;
        } else if(retVal >0){ /* au moins 1 socket a des éléments a faire */
            if (FD_ISSET(numSocket, &rfds)) { /* La socket serveur (de listen) a une nouvelle connexion */
                /* On accept la connexion */
                newNumSocket = h_accept(numSocket, serveurIP);
                printf(YELLOW"=============================================\n"NOCOLOR);
                printf(YELLOW"======== NEW CLIENT JOIN THE SERVER =========\n"NOCOLOR);
                printf(YELLOW"=============================================\n"NOCOLOR);
                
                //recup du pseudo : 
                printf(GREEN"Client pseudo is "NOCOLOR);
                h_reads(newNumSocket,pseudo,7);
                printf(GREEN"%s\n"NOCOLOR,pseudo);
                
                // TODO gerer si le client est deja connecter

                /* Ajout du nouveau client dans la liste des présents */ 
                char tmp = 1;
                if(getSockByPseudo(clientList,pseudo) == -1){
                    printf("Ajout du client %i %s\n",newNumSocket,pseudo); 
                    clientList = addList(clientList,newNumSocket,pseudo);
                    nbClientCourant ++;
                    h_writes(newNumSocket,&tmp,1);
                }else{
                    printf("Client %s deja existant\n",pseudo); 
                    h_close(newNumSocket);
                    tmp = 0;
                    h_writes(newNumSocket,&tmp,1);
                }

                /* ajout a l'interface */
                addPersonne(pseudo);
                sendMessagesConnect(newNumSocket,pseudo);
            }
            
            /* traitement des clients qui ont envoyé une requete */
            for(int i = 0; i < nbClientCourant;i++){
                /* traitement du client sock */
                int sock = getList(clientList,i);
                char * pseudoSockCourant = getListPseudo(clientList,sock);
                if (FD_ISSET(sock, &rfds)) {/* Si le client deriere la socket sock a envoyer une requete */
                    /* Récupération du code de la requete */
                    char codeAction;
                    
                    h_reads(sock,&codeAction,1);
                    /* Selon ce que veut le client */
                    switch (codeAction)
                    {
                    case 1:/* fin de connexion du client */
                        printf(YELLOW"=============================================\n"NOCOLOR);
                        printf(YELLOW"========== CLIENT LEAVES THE SERVER ===========\n"NOCOLOR);
                        printf(YELLOW"=============================================\n"NOCOLOR);
                        /* Fermeture de la connexion */
                        h_close(sock);
                        /* Ajout a la liste des suppression (pour éviter de suppr et d'iterer sur la meme liste) */
                        removeToClientList = addList(removeToClientList,sock,"NULL");
                        nbRemoveToClientList ++;
                        break;

                    case 2:
                        /* lecture du pseudo de la cible */
                        h_reads(sock,persoASub,7);
                        printf("Abonnement de %s à %s\n",pseudoSockCourant,persoASub);
                        addAbonnement(pseudoSockCourant,persoASub);
                        //si l'abonnement a pu se faire
                        char tmp;
                        if(existPersonneList(getAbonnes(persoASub),pseudoSockCourant)){
                            tmp = 1;
                            h_writes(sock,&tmp,1);//ok
                        }else{
                            tmp = 0;
                            h_writes(sock,&tmp,1);//pas ok
                        }
                        //printPersonneList(getAbonnements(pseudoSockCourant));
                        break;

                    case 3:
                        /* lecture du pseudo de la cible */ 
                        h_reads(sock,persoASub,7);
                        printf("Desabonnement de %s à %s\n",pseudoSockCourant,persoASub);
                        removeAbonnement(pseudoSockCourant,persoASub);
                        break;

                    case 4:
                        /* ecriture du message */
                        h_reads(sock,bufferText,21);
                        printf("Le texte recu de %s est : \n %s \n",pseudoSockCourant,bufferText);
                        sendMessage(sock,clientList,bufferText);
                        break;
                    case 5:
                        numSubs = nbAbonnements(pseudoSockCourant);
                        printf("Nombre d'abo : %i \n",numSubs);
                        h_writes(sock,(void*)&numSubs,4);
                        for(int i=0;i<numSubs;i++){
                            h_writes(sock,getPersonneListi(getAbonnements(pseudoSockCourant),i),7);
                            printf("Pseudo %i : %s \n",i,getPersonneListi(getAbonnements(pseudoSockCourant),i));
                        }
                        break;
                    default: /* Code non reconnu, théoriquement impossible */
                        printf(RED"Code non reconnu\n"NOCOLOR);
                        break;
                    }
                }
            }
            
            /* gestion des clients qui ont demandé la déconnexion (possiblement auucuns) */
            for(int i=0;i<nbRemoveToClientList;i++){
                nbClientCourant --;
                clientList = removeListSock(clientList,getList(removeToClientList,i));
            }
            /* On vide la liste des deconnexions */
            removeToClientList = videList(removeToClientList);
            nbRemoveToClientList = 0;

            /* On re-remplit la structure rfds (on pourrait utiliser bcopy) */
            FD_ZERO(&rfds);
            FD_SET(numSocket,&rfds); //ajout socket listen
            for(int i=0;i<nbClientCourant;i++){ // ajout de tous les clients
                //printf("Pass %i sock %i\n",i,getList(clientList,i));
                FD_SET(getList(clientList,i),&rfds);
            }
            //printf("OKKKKK");
            //printListConnecClient(clientList);
        }else{/* aucune socket disponible pour la lecture */
        }
	}

    printf(RED"Fermeture des connexions \n"NOCOLOR);
    h_close(numSocket);
    for(int i = 0; i < nbClientCourant;i++){
        h_close(getList(clientList,i));
    }
    FD_ZERO(&rfds);
}

/******************************************************************************/	
/**
 * It creates a socket, binds it to the given port, and listens for incoming connections
 * 
 * @param service the port number to use
 * @param serveurIP the server's IP address
 * 
 * @return The socket number.
 */
int connectServerClient(char *service, struct sockaddr_in *serveurIP){
    /* creation socket */
	int numSocket = h_socket(AF_INET, SOCK_STREAM);
	adr_socket(service, NULL, SOCK_STREAM, &serveurIP);
    /* bind */
	h_bind(numSocket, serveurIP);
    /* listen */
	h_listen(numSocket, 5);
    return numSocket;
}
/*****************************************************************************/

void sendMessage(int sockSrc, list clientList,char* buffMsg){
    char* pseudosrc = getListPseudo(clientList,sockSrc);
    //Ajout du msg a la structure
    addMessage(pseudosrc,buffMsg);
    listMessages msg = getMessagePseudo(pseudosrc,buffMsg);
    // Pour chaque personne co, on regarde si il est dans la liste
    list courant = clientList;
    int sockClient;
    while (courant != NULL)
    {
        //si la personne courante est dans la liste des destinataires du msg
        if(existPersonneList(msg->dest,courant->pseudo)){
            // On envoie le message
            sockClient = courant->socket;
            // On supprime de la liste des dest 
            removePersonneList(&(msg->dest),courant->pseudo);
            //envoie du msg
            printf("envoie de %s a %s\n",buffMsg,courant->pseudo);
            h_writes(sockClient,pseudosrc,7);
            h_writes(sockClient,buffMsg,21);
        }
        courant = courant->suiv;
    }
    //si plus aucuns destinataires on suppr le msg
    if(msg->dest == NULL){
        delMessage(pseudosrc);
    }
    
}


void sendMessagesConnect(int sockSrc,char* pseudo){
    //recuperation des abonnements de la personne :
    listPseudo aboList = getAbonnements(pseudo);

    //pour chaque personne, on regarde si elle a un message a dest de pseudo
    while(aboList != NULL){
        listMessages l = getMessages(aboList->pseudo);
        //pour chaque msg, on verif
        while(l!=NULL){
            //si le pseudo est dans la liste
            if(existPersonneList(l->dest,pseudo)){
                //envoie du msg
                h_writes(sockSrc,aboList->pseudo,7);
                h_writes(sockSrc,l->msg,21);
                //supression de la liste
                removePersonneList(&(l->dest),pseudo);
            }
            l = l->suivant;
        }
        if(l == NULL){
            delMessage(aboList->pseudo);
        }

        aboList = aboList->suiv;
    }
    //si vide, rien a faire
}