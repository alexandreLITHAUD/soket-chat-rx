#include "list.h"


/**
 * It adds a new cell to the list
 * 
 * @param l the list
 * @param c the socket
 * @param p the pseudo of the user
 * 
 * @return A pointer to a cell
 */
list addList(list l,int c,char* p){
    cell* cel = malloc(sizeof(cell));
    cel->socket = c;
    strcpy(cel->pseudo,p);
    if(l == NULL){
        cel->suiv = NULL;
    }else{
        cel->suiv = l;
    }
    //printf("ajout socket %d\n",c);
    return cel;
}

//suppr l'elem c de la list
/**
 * It removes the cell with the socket c from the list l
 * 
 * @param l the list
 * @param c the socket to remove
 * 
 * @return The list without the cell with the socket c
 */
list removeListSock(list l,int c){
    cell* courant = l;
    cell* prec = l;
    while (courant != NULL)
    {
        if(courant->socket == c){
            if(courant == prec){
                courant = courant->suiv;
                free(prec);
                return courant;
            }else{
                prec->suiv = courant->suiv;
                free(courant);
                return l;
            }
        }
        courant = courant->suiv;
    }
    printf("list.c : remove : la valeur n'est pas présente\n");
    return NULL;
}

list removeListPseudo(list l,char* pseudo){
    cell* c = l;
    cell* p = l;
    while(c != NULL){
        if(!(strcmp(pseudo,c->pseudo))){
            if(p == c){
                p = c->suiv;
                free(c);
                return p;
            }else{
                p->suiv = c->suiv;
                free(c);
                return l;
            }
        }
        p = c;
        c = c->suiv;
    }
    return l;
}

//donne l'elem pos i
int getList(list l,int i){
    cell* courant = l;
    int n=0;
    while(courant != NULL){
        if(i==n){
            return courant->socket;
        }
        n++;
        courant = courant->suiv;
    }
    printf("list.c : get : sortie de list\n");
    return -1;
}

//donne le pseudo a partir de la socket
char* getListPseudo(list l,int nSock){
    cell* courant = l;
    while (courant != NULL)
    {
        if(courant->socket == nSock){
            return courant->pseudo;
        }
        courant = courant->suiv;
    }
    printf(" Pseudo non trouvé a partir de la socket %i\n ",nSock);
    return NULL;
}

list videList(list l){
    if(l!=NULL){
        cell* c = l;
        while(c->suiv != NULL){
            c = c->suiv;
            free(l);
            l=c;
        }
        free(l);
    }
    return NULL;
}

void printListConnecClient(list l){
    cell* courant = l;
    while(courant != NULL){
        printf("Personne %s a socket %i\n",courant->pseudo,courant->socket);
        courant = courant->suiv;
    }
}


int getSockByPseudo(list l,char* p){
    list courant = l;
    while(courant != NULL){
        if(!strcmp(courant->pseudo,p)){
            return courant->socket;
        }
        courant = courant->suiv;
    }
    return -1;
}