#ifndef LIST_H
#define LIST_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct t_cell
{
    int socket;
    char pseudo[7];
    struct t_cell* suiv;
} cell;
typedef cell* list;

list addList(list l,int c,char* p);
list removeListSock(list l,int c);
list removeListPseudo(list l,char* p);
int getList(list l,int i);
char* getListPseudo(list l,int i);
int getSockByPseudo(list l,char* p);
list videList(list l);
void printListConnecClient(list l);

#endif