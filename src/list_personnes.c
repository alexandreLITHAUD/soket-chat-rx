#include "list_personnes.h"

void printPersonneList(listPseudo l){

    if(l == NULL){
        printf("Liste vide\n");
        return;
    }

    cellP* p = l;
    int i = 1;

    while (p != NULL)
    {
        printf("élement %i a pour pseudo %s\n",i,p->pseudo);
        i++;
        p = p->suiv;
    }
}


void addPersonneList(listPseudo* l,char* pseudo){
    //TODO 1 unique element
    cellP* cel = malloc(sizeof(cellP));
    strcpy(cel->pseudo,pseudo);
    cel->suiv = *l;
    *l = cel;
    //printf("ajout pseudo %s\n",cel->pseudo);
}

//suppr l'elem c de la list
void removePersonneList(listPseudo* l,char* pseudo){
    if(l == NULL || *l == NULL){
        printf("list_personnes.c : remove : liste vide\n");
        return;
    }

    cellP* cel = *l;
    cellP* prec = *l;
    while(cel != NULL){
        if(!strcmp(pseudo,cel->pseudo)){
            if(cel == prec){
                *l = cel->suiv;
                free(cel);
                printf("supr pseudo %s cas 1\n",pseudo);
                return;
            }else{
                prec->suiv = cel->suiv;
                free(cel);
                printf("supr pseudo %s\n",pseudo);
                return;
            }
        }
        prec = cel;
        cel = cel->suiv;
    }
    printf("list_personnes.c : remove : la valeur %s n'est pas présente\n",pseudo);
    return;
}

//donne l'elem pos i
int existPersonneList(listPseudo l,char* pseudo){
    cellP* courant = l;
    
    while(courant != NULL){
        //printf("pseudo : %s ET %s\n",courant->pseudo,pseudo);
        if(!strcmp(pseudo,courant->pseudo)){
            return 1;
        }
        courant = courant->suiv;
    }
    //printf("list_personnes.c : exist : sortie de list\n");
    return 0;
}

void videPersonneList(listPseudo* l){
    if(l != NULL || *l == NULL){
        listPseudo c = *l;
        while(c != NULL){
            listPseudo tmp = c;
            c = c->suiv;
            free(tmp);
        }
        *l = NULL;
    }
}

char* getPersonneListi(listPseudo l,int i){
    listPseudo courant = l;
    int tmp=0;
    while (courant != NULL)
    {
       if(tmp == i){
            return courant->pseudo;
       }
       tmp++;
       courant = courant->suiv;
    }
    printf("List_personnes.c : getPersonneListi : numero en dehors de la taille \n");
    return NULL;
}