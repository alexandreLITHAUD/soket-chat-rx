#include "interface.h"
 
listPersonnes inscrits;

/**
 * It prints the list of people who are registered
 * 
 * @return the number of people in the list.
 */
void printInscrits(){

    if(inscrits == NULL){
        printf("Aucun inscrit\n");
        return;
    }

    personne* p = inscrits;
    int i = 1;
    while (p != NULL)
    {
        printf("Inscrit %i a pour pseudo %s\n",i,p->pseudo);
        i++;
        p = p->persSuiv;
    }
}


/**
 * It prints the messages of a person
 * 
 * @param pseudo the pseudo of the person who posted the message
 */
void printMessages(char* pseudo){
    personne* p = getPersonne(pseudo);
    listMessages m = p->msg;
    while(m!=NULL){
        //printf("%s a posté %p->%p: \n",p->pseudo,m,m->suivant);
        printf("%s a posté : \n",p->pseudo);
        printf("%s\n",m->msg);
        m = m->suivant;
    }
}


/**
 * It returns a pointer to a personne structure, given a pseudo
 * 
 * @param pseudo the pseudo of the person we want to find
 * 
 * @return A pointer to a personne struct.
 */
personne* getPersonne(char* pseudo){
    listPersonnes courante = inscrits;

    while(courante != NULL){
        if(!strcmp(pseudo,courante->pseudo)){
            return courante;
        }
        courante = courante->persSuiv;
    }
    //printf("Personne non trouvée %s\n",pseudo);
    return NULL;
}

/**
 * It adds a person to the list of people
 * 
 * @param pseudo the name of the person to add
 * 
 * @return a pointer to the personne structure with the given pseudo.
 */
void addPersonne(char* pseudo){
    //si la personne est deja présente
    if(getPersonne(pseudo) != NULL){
        return;
    }

    //création de la structure Personne
    personne* temp = malloc(sizeof(personne));
    temp->abonnements = NULL;
    temp->abonnes = NULL;
    temp->msg = NULL;
    strcpy(temp->pseudo, pseudo);

    //Insertion en tête de liste
    temp->persSuiv = inscrits;
    //maj de la tete
    inscrits = temp;
}

/**
 * It frees the memory allocated for a person
 * 
 * @param p the person to free
 */
void libererPersonne(personne* p){
    //liberation des abonnements
    videPersonneList(&(p->abonnements));
    //liberation des abonnes
    videPersonneList(&(p->abonnes));

    //libération du message
    listMessages m = p->msg;
    listMessages tmp;
    //on parcours tous les msg 
    while(m != NULL){
        //on libere la liste de personnes du msg
        videPersonneList(&(m->dest));
        //on libere le msg lui meme et on passe au suivant
        tmp = m;
        m = m->suivant;
        free(tmp);
    }
    //on fini par liberer p
    free(p);
}

/**
 * It deletes a person from the list of people
 * 
 * @param pseudo the pseudo of the person to delete
 * 
 * @return a pointer to a listPersonnes.
 */
void delPersonne(char* pseudo){
    // Si il n'y a pas d'élément
    listPersonnes courant = inscrits;
    listPersonnes prec = inscrits;

    while(courant != NULL){
        if(!strcmp(pseudo,courant->pseudo)){//si les pseudo sont identiques
            if(prec == courant){
                inscrits = courant->persSuiv;
                libererPersonne(courant);
                return;
            }else{
                prec->persSuiv = courant->persSuiv;
                libererPersonne(courant);
                return;
            }
        }
        prec = courant;
        courant = courant->persSuiv;
    }
    printf("interface.c : delPersonne : sortie sans trouvée la personne %s\n",pseudo);
}

/**
 * It returns the list of messages of a person
 * 
 * @param pseudo the pseudo of the person you want to get the messages from
 * 
 * @return A list of messages
 */
listMessages getMessages(char* pseudo){
    return getPersonne(pseudo)->msg;
}

/**
 * It returns the list of subscriptions of a person
 * 
 * @param pseudo the pseudo of the person you want to get the subscriptions of
 * 
 * @return A list of pseudo
 */
listPseudo getAbonnements(char* pseudo){
    return getPersonne(pseudo)->abonnements;
};

/**
 * It returns the list of people who are subscribed to the person whose pseudo is given as a parameter
 * 
 * @param pseudo the pseudo of the person whose subscribers we want to get
 * 
 * @return A list of pseudo of the people who are following the person with the given pseudo.
 */
listPseudo getAbonnes(char* pseudo){
    personne* p = getPersonne(pseudo);
    if(p == NULL) return NULL;
    return p->abonnes;
}

/**
 * It adds a message to the list of messages of a person
 * 
 * @param pseudo the name of the person who sent the message
 * @param msgAenv the message to be sent
 */
void addMessage(char* pseudo,char msgAenv[21]){
    // Recupere la personne qui viens de mettre le message
    personne* tempPersonne = getPersonne(pseudo);
    if(tempPersonne == NULL){
        printf("interface.c : addMessage : impossible de trouver la personne %s\n",pseudo);
        exit(1);
    }

    listMessages messag = malloc(sizeof(message));
    if(messag == NULL){
        printf("ECHEC malloc : abort\n");
        exit(3);
    }
    //corps du msg
    strcpy(messag->msg,msgAenv);
    //list des destinataires reverse la liste mais pas grave
    messag->dest = NULL;
    
    listPseudo courant = tempPersonne->abonnes;
    while(courant != NULL){
        addPersonneList(&(messag->dest),courant->pseudo);
        courant = courant->suiv;
    }
    //printf("Destinataires du msg %s : \n",msg);
    //maj de la liste dans la personne
    messag->suivant = tempPersonne->msg;
    tempPersonne->msg = messag;
    
    //printf("msg %s de %s : tempPerson pointe sur %p qui pointe sur %p\n",messag->msg,pseudo,tempPersonne->msg,messag->suivant);
}

/**
 * It adds an abonnement between two people
 * 
 * @param pseudoAbonne the name of the person who is subscribing to the other person
 * @param pseudoCible the person who is being followed
 * 
 * @return a pointer to the personne structure.
 */
void addAbonnement(char* pseudoAbonne, char* pseudoCible){
    personne* tempAbo = getPersonne(pseudoAbonne);
    personne* tempCible = getPersonne(pseudoCible);

    if (tempAbo == NULL || tempCible == NULL){
        printf("L'abonne ou la cible n'existe pas !\n");
        return;
    }
    //si l'abonement existe
    if(existPersonneList(tempAbo->abonnements,pseudoCible)){
        printf("L'abonne existe deja !\n");
        return;
    }
    //printf("Ajout de %s et %s\n",pseudoAbonne,pseudoCible);

    //Ajout à la liste d'abonnements de l'abonné
    addPersonneList(&(tempAbo->abonnements),pseudoCible);

    //Ajout à liste d'abonnés de la cible
    addPersonneList(&(tempCible->abonnes),pseudoAbonne);
}

//remove l'abonnement de pseudoAbonne a pseudoCible
/**
 * It removes an abonnement from the list of abonnements of a person
 * 
 * @param pseudoAbonne the pseudo of the person who wants to subscribe
 * @param pseudoCible the person who is being followed
 */
void removeAbonnement(char* pseudoAbonne, char* pseudoCible){
    personne* rAbonne = getPersonne(pseudoAbonne);
    personne* rAbonnement = getPersonne(pseudoCible);
    //suppression de l'abonné a la liste des abonnées de la cible
    removePersonneList(&(rAbonnement->abonnes),pseudoAbonne);

    //suppression de l'abonnement dans la liste des abo du pseudoAbo
    removePersonneList(&(rAbonne->abonnements),pseudoCible);
}


/**
 * It returns the number of subscriptions of a given user.
 * 
 * @param nom the name of the user
 */
int nbAbonnements(char* nom){
    personne* pers =  getPersonne(nom);
    listPseudo abo = pers->abonnements;
    int nb = 0;
    while(abo != NULL){
        nb++;
        abo = abo->suiv;
    }
    return nb;
}

/**
 * It returns the message with the given content from the given person
 * 
 * @param pseudo the pseudo of the person who sent the message
 * @param msg the message to be found
 * 
 * @return A pointer to a message
 */
listMessages getMessagePseudo(char* pseudo,char* msg){
    personne* pers =  getPersonne(pseudo);
    listMessages m = pers->msg;
    while(m != NULL){
        if(!strcmp(msg,m->msg)){
            return m;
        }
        m = m->suivant;
    }
    printf("interface.c : getMessagepseudo : sortie sans trouver le msg\n");
    return NULL;
}

/**
 * It deletes a message from the database.
 * 
 * @param pseudo the pseudo of the user who sent the message
 */
void delMessage(char* pseudo){
    personne* pers =  getPersonne(pseudo);
    listMessages courant = pers->msg;
    listMessages prec = courant;
    while(courant != NULL){
        if(courant->dest == NULL){
            if(prec == courant){
                pers->msg = courant->suivant;
                free(courant);
                return;
            }else{
                prec->suivant = courant->suivant;
                free(courant);
                return;
            }
        }

        courant = courant->suivant;
    }
}