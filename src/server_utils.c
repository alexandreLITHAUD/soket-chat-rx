#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include "server_utils.h"
#include "fon.h"

int init_comm_serv(char* service, struct sockaddr_in *serveurIP)
{
    /* creation de la socket passive en IPV4 et TCP */
	int numSock = h_socket(AF_INET, SOCK_STREAM);
    /* Définition de la structure pour le resutat du getaddrInfo */
    //struct sockaddr_in *s;
    /* remplir la sockaddr_in avec comme port : service, comme IP, toutes celles de la machine et en mode TCP */
	adr_socket(service, NULL, SOCK_STREAM, &serveurIP);

    /* Structure qui contiendra les infos du client connecter */
	struct sockaddr_in *clientInfo;
	int numSockClient;

	/* bind de la socket avec le contenu de sockaddr_in */
	h_bind(numSock, serveurIP);

	/* listen des connexions */
	h_listen(numSock, 5);

    
}