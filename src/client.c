#include <stdio.h>
#include <curses.h> 		/* Primitives de gestion d'ecran */
#include <sys/signal.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h> 

#include "fon.h"   		/* primitives de la boite a outils */

#define SERVICE_DEFAUT "9999"
#define SERVEUR_DEFAUT "127.0.0.1"

#define RED "\033[0;31m"
#define YELLOW "\033[0;33m"
#define GREEN "\033[0;32m"
#define BLUE "\033[0;34m"
#define NOCOLOR "\033[0m"

int connectClientServer(char *serveur, char *service);
void client_appli (char *serveur, char *service);
char *parse_pseudo(char * b);
void gererConnexion(int numSock);
char parse_entree_code();

/* Contient les arguments des commandes en demandant (ex : pour sub, contient le nom du compte) */
char optionsText[50];

/*****************************************************************************/
/*--------------- programme client -----------------------*/

int main(int argc, char *argv[])
{
	char *serveur= SERVEUR_DEFAUT; /* serveur par defaut */
	char *service= SERVICE_DEFAUT; /* numero de service par defaut (no de port) */

	/* Permet de passer un nombre de parametre variable a l'executable */
	switch(argc)
	{
 	case 1 :		/* arguments par defaut */
		  printf("serveur par defaut: %s\n",serveur);
		  printf("service par defaut: %s\n",service);
		  break;
  	case 2 :		/* serveur renseigne  */
		  serveur=argv[1];
		  printf("service par defaut: %s\n",service);
		  break;
  	case 3 :		/* serveur, service renseignes */
		  serveur=argv[1];
		  service=argv[2];
		  break;
    default:
		  printf("Usage:client serveur(nom ou @IP)  service (nom ou port) \n");
		  exit(1);
	}

	/* serveur est le nom (ou l'adresse IP) auquel le client va acceder */
	/* service le numero de port sur le serveur correspondant au  */
	/* service desire par le client */
	
	client_appli(serveur,service);
}

/*****************************************************************************/
/**
 * It connects to the server, sends the pseudo, and then enters a loop that waits for the users inputs
 * 
 * @param serveur the server's IP address
 * @param service the name of the service (port) to connect to.
 */
void client_appli (char *serveur,char *service)
{
	int numSocket;
	int running = 1;

	numSocket = connectClientServer(serveur, service);

	printf("Connected !!\n");

    /* GESTION du pseudo */
	printf(YELLOW"Please Write you pseudonyme ! (6 characters max)\n"NOCOLOR);
	char b[7];
    char* pseudo = parse_pseudo(b);
    /* ecriture du pseudo */
	h_writes(numSocket, pseudo, 7);
    char tmp;
    h_reads(numSocket,&tmp,1);
    if(tmp == 0){
        printf(RED"Connexion impossible, %s est deja connecter sur le serveur \n"NOCOLOR,pseudo);
        h_close(numSocket);
        exit(1);
    }

	/* boucle de connexion du client */
    gererConnexion(numSocket);

    /* Fin de connexion */
    h_close(numSocket);

}
/*****************************************************************************/

/**
 * It reads the user's input and sends the corresponding code to the server
 * 
 * @param numSock The socket number
 */
void gererConnexion(int numSock){
    char finConnexion = 0;
    char code;
    char tmp;

    int nbSub;
    char bufferSub[7];
    char msgRecu[21];
    char pseudoMsg[7];

    /* Init pour le select */
    /*Structure*/
    fd_set rfds;
    FD_ZERO(&rfds);//mise a 0
    FD_SET(numSock,&rfds); // ajout socket de connexion
    FD_SET(0,&rfds); // ajout socket de connexion
    int retVal;

    //affichage du msg d'aide
    printf(GREEN"Que voulez vous faire :\n - Quitter -> quit\n - S'abonner -> sub\n - Se desabonner -> unsub\n - Ecrire un message -> write \n - Lister les abonnements -> list\n - help -> avoir de l'aide\n"NOCOLOR);

    /* Tq le client ne demande pas la deconnexion */
    while(!finConnexion){
        // att socket ou l'on peut lire
        retVal = select(FD_SETSIZE,&rfds,NULL,NULL,NULL);
        if(retVal<0){/* Echec du select */
            perror("Err Select");
            break;
        }else if(retVal>0){
            if (FD_ISSET(0, &rfds)) {//entree sur stdin
                /* On réupére un code correspondant a la commande qu'entre l'utilisateur */
                code = parse_entree_code();
                /* On écrit le code */
                h_writes(numSock,&code,1);

                /* Actions a réaliser en fonction du code */
                switch (code)
                {
                case 0://affichage de l'aide
                    break;
                case 1: /* Demande de deconnexion */
                    finConnexion = 1;
                    printf(YELLOW"\nFin de connexion !\n"NOCOLOR);
                    break;

                case 2: /* abonnement */
                    printf("Vous vous abonnez a %s : ",optionsText);
                    // ecriture du pseudo dest
                    h_writes(numSock,optionsText,7);
                    // recupération du resultat de l'opération
                    h_reads(numSock,&tmp,1);
                    if(tmp){
                        printf(GREEN"Réussite \n"NOCOLOR);
                    }else{
                        printf(RED"Echec, aucun compte\n"NOCOLOR);
                    }
                    break;

                case 3: /* Desabonnement */
                    printf("Vous vous désabonnez à %s\n",optionsText);
                    h_writes(numSock,optionsText,7);
                    break;

                case 4: /* Envoyer un message */
                    printf("Vous envoyez : %s\n",optionsText);
                    h_writes(numSock,optionsText,21);
                    break;

                case 5: /* Liste des abonnées */
                    printf(YELLOW"Liste abonnements :\n"NOCOLOR);
                    h_reads(numSock,(void*)&nbSub,4);

                    if(nbSub == 0){
                        printf("Nous n'avez pas d'abonnements !\n");
                    }
                    else{
                        //pour chaque abonné, on l'affiche
                        for(int i = 0;i<nbSub;i++){
                            h_reads(numSock,bufferSub,7);
                            printf(GREEN"%s\n"NOCOLOR,bufferSub);
                        }
                    }
                    break;
                default: /* Code inconnu, ne devrait pas arriver normalement si le code n'est pas connu c'est la fonction de parsing qui leve l'exception */
                    fprintf(stderr,"client.c : gererConnexion : ne devrait pas passer la !");
                    break;
                }
            }
            // si le serveur nous envoie un message 
            if(FD_ISSET(numSock, &rfds)){
                //affichage pseudo
                h_reads(numSock,pseudoMsg,7);
                printf(RED"%s > ",pseudoMsg);
                //affichage msg
                h_reads(numSock,msgRecu,21);
                printf("%s\n"NOCOLOR,msgRecu);
            }

            //reset de la structure
            FD_ZERO(&rfds);
            FD_SET(numSock,&rfds); //ajout socket listen
            FD_SET(0,&rfds); //ajout socket listen
        }else{

        }
        

    }
}

/*****************************************************************************/


/**
 * It creates a socket, connects to the server, and returns the socket number
 * 
 * @param serveur the server's IP address
 * @param service the port number of the server
 * 
 * @return The socket number.
 */
int connectClientServer(char *serveur, char *service){
    /* creation socket */
	int numSocket = h_socket(AF_INET, SOCK_STREAM);
    /* infos du serveur */
	struct sockaddr_in *serverIP;
	adr_socket(service, serveur, SOCK_STREAM, &serverIP);

    /* connexion au serveur */
	h_connect(numSocket, serverIP);
    return numSocket;
}
/*****************************************************************************/

/**
 * It asks the user to enter a pseudo, and returns it
 * 
 * @param b the buffer where the pseudo will be stored
 * 
 * @return The pseudo of the client.
 */
char *parse_pseudo(char * b)
{
    do
    {
        printf(YELLOW"Entrez un pseudo (6 characters max) :\n"NOCOLOR);
        int res = scanf("%s", b);
        if (res != 1) /* si scanf echoue */
        {
            fprintf(stderr, "client.c : parse_pseudo : Erreur scanf !\n");
            exit(1);
        }
    } while (strlen(b) >= 7 || strlen(b) == 0);
    printf(BLUE"Votre pseudo est : %s\n\n"NOCOLOR, b);
	return b;
}

/*****************************************************************************/

/**
 * It asks the user what he wants to do, and returns the code corresponding to the user's choice
 * 
 * @return a char.
 */
char parse_entree_code()
{
    char codeString[50];
    char code = -1;
    do
    {
        int res = scanf("%s", codeString);
        if (res != 1)/* si scanf echoue */
        {
            fprintf(stderr, "client.c : parse_entree_code : Erreur scanf !\n");
            return 1;
        }

        /* l'utilisateur veut quitter : Quit/quit/Q/q */
        if(!strcmp(codeString,"Quit") || !strcmp(codeString,"quit") || !strcmp(codeString,"q") || !strcmp(codeString,"Q")){
            printf(YELLOW"\nVous avez choisi de quitter !\n"NOCOLOR);
            code = 1;
        }

        /* l'utilisateur veut s'abonner : Subscribe/subscribe/sub/Sub/s/S */
        if(!strcmp(codeString,"Subscribe") || !strcmp(codeString,"subscribe") || !strcmp(codeString,"s") || !strcmp(codeString,"S") || !strcmp(codeString,"sub") || !strcmp(codeString,"Sub")){
            //printf(YELLOW"\nVous avez choisi de vous abonner !\n"NOCOLOR);
            printf("A qui voulez-vous vous abonner ?\n");
            res = scanf("%7s", optionsText);
            if (res != 1)/* si scanf echoue */
            {
                fprintf(stderr, "client.c : parse_entree_code : Erreur scanf subscribe !\n");
                return 1;
            }
            code = 2;
        }

        /* l'utilisateur veut se desabonner : Unsubscribe/unsubscribe/unsub/Unsub/u/U */
        if(!strcmp(codeString,"Unsubscribe") || !strcmp(codeString,"unsubscribe") || !strcmp(codeString,"u") || !strcmp(codeString,"U") || !strcmp(codeString,"unsub") || !strcmp(codeString,"Unsub")){
            //printf(YELLOW"\nVous avez choisi de vous désabonner !\n"NOCOLOR);
            printf("A qui voulez-vous vous désabonner ?\n");
            res = scanf("%7s", optionsText);
            if (res != 1)/* si scanf echoue */
            {
                fprintf(stderr, "client.c : parse_entree_code : Erreur scanf unsubscribe !\n");
                return 1;
            }
            code = 3;
        }

        /* l'utilisateur veut envoyer un message : write/Write/w/W */
        if(!strcmp(codeString,"Write") || !strcmp(codeString,"write") || !strcmp(codeString,"w") || !strcmp(codeString,"W")){
            //printf(YELLOW"\nVous avez choisi d'écrire un message !\n"NOCOLOR);
            printf("Que voulez-vous écrire ?\n");
            res = scanf(" %[^\n]s",optionsText);
            if (res != 1)/* si scanf echoue */
            {
                fprintf(stderr, "client.c : parse_entree_code : Erreur scanf write !\n");
                return 1;
            }
            code = 4;
        }

        /* l'utilisateur veux la liste des personnes auquel il est abboné : list/List/l/L */
        if(!strcmp(codeString,"List") || !strcmp(codeString,"list") || !strcmp(codeString,"l") || !strcmp(codeString,"L")){
            //printf(YELLOW"\nVous avez choisi de recupere votre liste d'abonnée !\n"NOCOLOR);
            code = 5;
        }

        if(!strcmp(codeString,"Help") || !strcmp(codeString,"help") || !strcmp(codeString,"h") || !strcmp(codeString,"H")){
            printf(GREEN"Que voulez vous faire :\n - Quitter -> quit\n - S'abonner -> sub\n - Se desabonner -> unsub\n - Ecrire un message -> write \n - Lister les abonnements -> list\n - help -> avoir de l'aide\n"NOCOLOR);
            code = 0;
        }

    } while (code == -1); /* TQ on a pas un code valide */
	return code;
}
