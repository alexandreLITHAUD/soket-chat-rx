#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include "client_utils.h"
#include "fon.h"

/**
 * It asks the user to enter a pseudo, and returns it
 *
 * @return A pointer to the first character of the string.
 */
char *parse_pseudo(char * b)
{
    do
    {
        printf("Entrez un pseudo (6 characters max) :\n");
        int res = scanf("%s", b);
        if (res != 1)
        {
            fprintf(stderr, "Fon.c : parse_pseudo : Erreur scanf !\n");
            exit(1);
        }
    } while (strlen(b) > 7);
    printf("\n Votre pseudo est : %s\n", b);
    return b;
}

int init_comm_client(char *serveur,char *service)
{
    /* Création de la socket */
    int numSock = h_socket(AF_INET,SOCK_STREAM);
    /* Définition de la structure pour le resutat du getaddrInfo */
    struct sockaddr_in* sockServ;
    /* Surcouche getAddrInfo */
    adr_socket(service,serveur,SOCK_STREAM,&sockServ);
    /* Connection du client */
    h_connect(numSock,sockServ);
}