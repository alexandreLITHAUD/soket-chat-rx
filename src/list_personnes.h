#ifndef LIST_PERSONNES_H
#define LIST_PERSONNES_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct t_cell_p
{
    char pseudo[7];
    struct t_cell_p* suiv;
} cellP;
typedef cellP* listPseudo;

void printPersonneList(listPseudo l);
void addPersonneList(listPseudo* l,char* pseudo);
void removePersonneList(listPseudo* l,char* pseudo);
int existPersonneList(listPseudo l,char* pseudo);
void videPersonneList(listPseudo* l);
char* getPersonneListi(listPseudo l,int i);

#endif