#ifndef INTERFACE_H
#define INTERFACE_H

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "list_personnes.h"

typedef struct t_message
{
    char msg[21];
    listPseudo dest;
    struct t_message* suivant;
} message;
typedef message *listMessages;

typedef struct t_personne
{
    char pseudo[7];
    listPseudo abonnements;
    listPseudo abonnes;
    listMessages msg;

    struct t_personne* persSuiv;
} personne;
typedef personne *listPersonnes;

//si t null : depuis tjr
personne* getPersonne(char* pseudo);

listMessages getMessages(char* pseudo);

listPseudo getAbonnements(char* pseudo);

listPseudo getAbonnes(char* pseudo);

void addMessage(char* pseudo,char msg[21]);

void addAbonnement(char* pseudoAbonne, char* pseudoCible);

void removeAbonnement(char* pseudoAbonne, char* pseudoCible);

void addPersonne(char* pseudo);

void delPersonne(char* pseudo);

void printInscrits();

void printMessages(char* nom);

int nbAbonnements(char* nom);

listMessages getMessagePseudo(char* pseudo,char* msg);

void delMessage(char* pseudo);
//void notifyMessage(listPersonne abonnes, messages msg);   -> serveur

#endif